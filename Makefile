#Camille Bowman (cbowma19): cbowma19@jhu.edu
# 601.220 HW #4 due 9/26/2017

CC=gcc
CFLAGS=-std=c99 -pedantic -Wall -Wextra -g 

# Links together files needed to create executable
main: main.o menu_option.o read.o write.o img_manip.o adjust.o 
	$(CC) main.o menu_option.o read.o write.o img_manip.o adjust.o -o main -lm

# Compiles main.c to create main.o
# Functions main.c depends on: menu_option.h
main.o: main.c menu_option.h 
	$(CC) $(CFLAGS) -c main.c

# Compiles menu_option.c to create menu_option.o
# Functions menu_option.c depends on: menu_option.h read.h write.h structs.h img_manip.h adjust.h
menu_option.o: menu_option.c menu_option.h read.h write.h structs.h img_manip.h adjust.h
	$(CC) $(CFLAGS) -c menu_option.c

# Compiles read.c to create read.o
# Functions read.c depends on: read.h structs.h
read.o: read.c read.h structs.h
	$(CC) $(CFLAGS) -c read.c 

# Compiles write.c to create write.o
# Functions write.c depends on: write.h structs.h
write.o: write.c write.h structs.h
	$(CC) $(CFLAGS) -c write.c

# Compiles adjust.c to create adjust.o
# Functions adjust.c depends on: adjust.h structs.h img_manip.c
adjust.o: adjust.c adjust.h structs.h img_manip.c
	$(CC) $(CFLAGS) -c adjust.c

# Compiles img_manip.c to create img_manip.o
# Functions img_manip.c depends on: img_manip.h, structs.h
img_manip.o: img_manip.c img_manip.h structs.h
	$(CC) $(CFLAGS) -c img_manip.c

# Removes all object files and the executable named db,
# so we can start fresh
#clean:
#	rm -f *.o db
