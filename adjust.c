/** Camille Bowman (cbowma19): cbowma19@jhu.edu
 *  William Hartmann (whartma2): whartma2@jhu.edu
 *
 *  601.220 Midterm Project due 10/16/2017
 *  File for grayscale, contrast, edge detection, and sharpen 
 */
#include <stdlib.h>
#include <stdio.h>
#include "structs.h"
#include <math.h>
#include "adjust.h"
#include "img_manip.h"
/*cap function returns the parameter capped between 0 and 255
 *@param tocap the integer to cap between 0 and 255
 */
unsigned char cap(int tocap){
  if(tocap > 255){return 255;}
  if(tocap < 0){return 0;}
  return tocap;
}
/*adjust 
 *@param change the integer to 
 
img* adjust(int change,img* myimg){
  for(int i = 0; i < (myimg->x)*(myimg->y); i++){
    myimg->pixels[i].r = cap(myimg->pixels[i].r + change);
  
    myimg->pixels[i].g = cap(myimg->pixels[i].g + change);
    
    myimg->pixels[i].b = cap(myimg->pixels[i].b + change);

  }
  return myimg;
  }*/
/*converts the pixels in myimg to grayscale using the NTSC formula
 *@param myimg the img to convert to grayscale
 */
img* grayscale(img* myimg){
  for(int i = 0; i < (myimg->x)*(myimg->y); i++){
     int intensity = 0.3*myimg->pixels[i].r +  0.59*myimg->pixels[i].g + 0.11*myimg->pixels[i].b; 
     myimg->pixels[i].r = intensity;
     myimg->pixels[i].g = intensity;
     myimg->pixels[i].b = intensity;
   }
  return myimg;
}
/*Shift the value in cr to the range [-255/2, 255/2]
 *Then multiply it by the factor to shift by
 *Then shift it back
 *@param cr the number to shift
 *@factor the number to multiply by
 */ 
double contrastHelper(double cr, double factor){
  //double cr = (myimg->pixels[i].r - 255.0/2.0);
  cr = cr - 255.0/2.0;
     cr = cr*factor;
     cr = cr + 255.0/2.0;
     cr = cap(cr);
     return cr;
}
/*Adjusts the contrast by the given value
 *@param factor the factor to adjust by
 *@param myimg the img to adjust the contrast on
 */
img* contrast(double factor, img* myimg){
   for(int i = 0; i < (myimg->x)*(myimg->y); i++){
   
     myimg->pixels[i].r =(int) contrastHelper(myimg->pixels[i].r, factor);
     myimg->pixels[i].g = (int)contrastHelper(myimg->pixels[i].g, factor);
     myimg->pixels[i].b = (int)contrastHelper(myimg->pixels[i].b, factor);
   }
   return myimg;
}
/*Crops the img from the (tlx,tly) to (brx, bry)
 *The matrix is indexed 0 to x left to right, and 0 to y top to bottom
 *brx > tlx, bry > tly
 *includes the edges
 */
img* crop(img* myimg, int tlx, int tly, int brx, int bry){
  img* new = malloc(sizeof(img));
  new->valid = 1;
  if((tlx < 0) || (tlx > myimg->x) || (tly < 0) || (tly > myimg->y) || (brx < 0) || (brx > myimg->x) || (bry < 0) || (bry > myimg->y) || (tlx > brx) || (tly > bry)){
    //bad img option here
    fprintf(stderr, "Inputs are not in range\n");
    return myimg;
  }else{
    //    img* new = malloc(sizeof(img));
    new->x = (brx - tlx +1);
    new->y = (bry - tly +1);
    new->pixels = (Pixel *)malloc(new->x*new->y*3);
    int j = 0;
    for(int i = 0; i < myimg->x * myimg->y; i++){
      if((i%myimg->x >= tlx) && (i%myimg->x <= brx) && ((i - i%myimg->x)/myimg->x >= tly) &&  ((i - i%myimg->x)/myimg->x <= bry)){
	new->pixels[j] = myimg->pixels[i];
	j++;
      }
    }
  }
  free(myimg->pixels);
  free(myimg);
  return new;
}

/*edge detection using sigma to apply the sigma filter and threshold to set how strong the edges must be before they're detected.
 *
 */

img* edge(img* myimg, double sigma, double threshold){
  myimg = grayscale(myimg);
  myimg = blur(myimg, sigma);
  img* cp = copy(myimg);
  for(int i = 0; i < (myimg->x*myimg->y); i++){
    double dx = 0;
    double dy = 0;
    int row = i / myimg->x;
    int col = i % myimg->x;
    if((row != 0) && (row < myimg->y)&& (col != 0) && (col < myimg->x)){
      dx = ((cp->pixels[i + 1].r - cp->pixels[i - 1].r)/2.0);
      dy = ((cp->pixels[i + cp->x].r - cp->pixels[i - (cp->x)].r)/2.0);
    }
    double grad = sqrt(pow(dx,2.0) + pow(dy,2.0));
    if(grad > threshold){
      myimg->pixels[i].r = 0;
      myimg->pixels[i].g = 0;
      myimg->pixels[i].b = 0; 
    }else{
      myimg->pixels[i].r = 255;
      myimg->pixels[i].g = 255;
      myimg->pixels[i].b = 255;
    }
  }
  free(cp->pixels);
  free(cp);
  return myimg;
}

img* copy(img* myimg){
  img* blurred = (img*)malloc(sizeof(img));
  blurred->x = myimg->x;
  blurred->y = myimg->y;
  blurred->pixels = (Pixel*)malloc(sizeof(Pixel)*myimg->x*myimg->y);
  //Pixel* copy = blurred->pixels;
  for(int i = 0; i < (myimg->x*myimg->y); i++){
    blurred->pixels[i].r = myimg->pixels[i].r;
    blurred->pixels[i].g = myimg->pixels[i].g;   
    blurred->pixels[i].b = myimg->pixels[i].b;
  }
  return blurred;
}

img* sharpen(img* myimg, double sigma, double intensity){
  img* blurred = copy(myimg);
  blurred = blur(blurred,sigma);
  for(int i = 0; i < (myimg->x*myimg->y); i++){
    myimg->pixels[i].r = cap(myimg->pixels[i].r + intensity*(myimg->pixels[i].r - blurred->pixels[i].r));
    myimg->pixels[i].g = cap(myimg->pixels[i].g + intensity*(myimg->pixels[i].g - blurred->pixels[i].g));
    myimg->pixels[i].b = cap(myimg->pixels[i].b + (intensity*(myimg->pixels[i].b - blurred->pixels[i].b)));
  }
  free(blurred->pixels);
  free(blurred);
  return myimg;
}
