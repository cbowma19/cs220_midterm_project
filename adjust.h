/** Camille Bowman (cbowma19): cbowma19@jhu.edu
 *  William Hartmann (whartma2): whartma2@jhu.edu
 *
 *  601.220 Midterm Project due 10/16/2017
 *  Header file for adjust.c 
 */

#include "structs.h"

unsigned char cap(int tocap);

img* grayscale(img* myimg);

double contrastHelper(double cr, double factor);

img* contrast(double factor, img* myimg);

img* crop(img* myimg, int tlx, int tly, int brx, int bry);

img* edge(img* myimg, double sigma, double threshold);

img* copy(img* myimg);

img* sharpen(img* myimg, double sigma, double intensity);

