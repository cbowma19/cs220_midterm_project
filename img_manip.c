/** Camille Bowman (cbowma19): cbowma19@jhu.edu
 *  William Hartmann (whartma2): whartma2@jhu.edu
 *
 *  601.220 Midterm Project due 10/16/2017
 *  File for brightness, swap, and blur
 */

#include <stdio.h>
#include <stdlib.h>
#include "structs.h"
#include "img_manip.h"
#include <math.h>
#include "adjust.h"
#define UPPER 255
#define LOWER 0
#define PI 3.1416
#define sq(X) X * X

/** Function to hold the value of a pixel within [0, 255]
 *  @param temp the double to check
 *  @return the value within the range
 */
unsigned char hold(double temp) {
	if (temp > UPPER) {
		return (unsigned char) 255;
	}
	else if (temp < 0) {
		return (unsigned char) 0;
	}
	else {
		return (unsigned char) temp;
	}
}

/** Function to swap the colors of a file
 *  @param im the image to alter
 *  @return a pointer to the swapped image
 */
img* swap(img *im) {
	Pixel *pix = im->pixels;
	int i = im->x * im->y;
	for (int j = 0; j < i; j++, pix++) {
		char temp = pix->r;
		pix->r = pix->g;
		pix->g = pix->b;
		pix->b = temp;
	}	
	return im;
}

/** Function to adjust a pixel by a given amount,
 *  keeping it in range [0, 255]
 *  @param c the current pixel color value
 *  @param amt the amount to adjust it by
 *  @return the new pixel value
 */
unsigned char adjust(unsigned char c, double amt) {
	double temp = (double) c;
	temp = temp + amt;
	return hold(temp);
}	

/** Function to increase/decrease an image's brightness
 *  @param im the image to alter
 *  @param amt the amount to change the brightness by
 *  @return the altered image
 */
img* brightness(img *im, double amt) {
	Pixel *pix = im->pixels;
	int i = im->x * im->y;
	for (int j = 0; j < i; j++, pix++) {
		pix->r = adjust(pix->r, amt);
		pix->g = adjust(pix->g, amt);
		pix->b = adjust(pix->b, amt);	
	}
	return im;
}
/** Function to find the necessary size of the Gaussian matrix
 *  @param sig the sigma used
 *  @return the dimension
 */
int find_gauss_size(double sig) {
	int n = sig * 10;
	double temp = (double) n;
	if (temp < sig * 10.0) {
		n++;
	}
	if (n % 2 == 0) {
		n++;
	}
	return n;
}

/** Function to take a sum of all elements a 2D matrix of doubles
 *  @param avg matrix of doubles
 *  @param rs the row to start at
 *  @param rf the row to finish at
 *  @param cs the column to start at
 *  @param cf the column to finish at
 *  @return the sum of all elements 
 */
double sum(double **avg, int rs, int rf, int cs, int cf) {
	double total = 0;
	for (int i = rs; i < rf; i++) {
		for (int j = cs; j < cf; j++) {
			total += avg[i][j];
		}
	}
	return total;
}

/** Function to create the Gaussian matrix
 *  @param sig the sigma used to make the matrix
 *  @param n the dimension of the NxN Gaussian Matrix
 *  @return the Gaussian matrix with correct values
 */
double** create_gaussian(double sig, int n) {
	double **gauss = (double **)malloc(n * sizeof(double *));
	for (int i = 0; i < n; i++) {
		gauss[i] = (double *)malloc(n * sizeof(double));
	}
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++) {
			int dx = abs(j - (n / 2));
			int dy = abs(i - (n / 2));
			gauss[i][j] = (1.0 / (2.0 * PI * sq(sig))) * exp(-((sq(dx) + sq(dy)) / (2.0 * sq(sig))));
		}
	}
	return gauss;
}

/** Function to normalize a pixel
 *  @param pix the pixel
 *  @param sum_r the sum of weighted r values
 *  @param sum_g the sum of weighted g values
 *  @param sum_b the sum of weighted b values
 *  @param g_sum the sum of all valid indices in the Gaussian matrix
 */
void normalize_pix(Pixel *pix, double sum_r, double sum_g, double sum_b, double g_sum) {
	pix->r = hold(sum_r / g_sum);
	pix->g = hold(sum_g / g_sum);
	pix->b = hold(sum_b / g_sum);
	//printf("%uc %uc %uc\n", pix->r, pix->g, pix->b);
}

/** Function to blur one pixel of an image
 *  @param gauss the Gaussian matrix
 *  @param pix the pixel to blur
 *  @param j the index of the pixel in the image
 *  @param c the number of columns of pixels in the image
 *  @param r the number of rows of pixels in the image
 *  @param n the number of rows/columns in the Gaussian matrix
 *  @param temp the pixel corresponding to pix in the copy of the image
 *  @return the blurred pixel
 */
void blur_pix(double **gauss, Pixel *pix, int j, int c, int r, int n, Pixel *temp) {
	double sum_r = 0, sum_g = 0, sum_b = 0;
	int n1 = n / 2;
	int row = j / c;
	int col = j % c;
	double g_sum;
	if (row + n1 < r && row - n1 >= 0 && col + n1 < c && col - n1 >= 0) {
		//go to the start of the gaussian matrix in the image
		for (int i = 0; i < n1 * c + n1; i++, temp--);
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				sum_r += gauss[i][j] * temp->r;		
				sum_g += gauss[i][j] * temp->g;		
				sum_b += gauss[i][j] * temp->b;
				temp++;
			}
			//go to the start of the next row of the gaussian matrix in the image
			for (int j = 0; j < c - n; j++, temp++);
		}
		g_sum = sum(gauss, 0, n, 0, n);
		normalize_pix(pix, sum_r, sum_g, sum_b, g_sum);
	}
	else if (col + n1 < c && col - n1 >= 0) {
	//	printf("iterating (missing rows)\n");
		int lost_top = 0;
		int lost = (row + n1) - r + 1;
		if (lost <= 0) {
			lost = (n1 - row);
			lost_top = 1;	
		}
		int r_valid = n - lost;
	//	printf("%d %d %d %d %d", row, col, lost, r_valid, lost_top);
		if (lost_top) {
			g_sum = sum(gauss, 0, r_valid, 0, n); 
		}
		else {
			g_sum = sum(gauss, lost, n, 0, n);
		}	
	//	printf(" %f\n", g_sum);
		// Back up to start of valid matrix
		for (int i = 0; i < (n1 - (lost * lost_top)) * c + n1; i++, temp--);
		for (int i = 0; i < r_valid; i++) {
			for (int j = 0; j < n; j++) {
				sum_r += gauss[i + (lost * lost_top)][j] * temp->r;		
				sum_g += gauss[i + (lost * lost_top)][j] * temp->g;		
				sum_b += gauss[i + (lost * lost_top)][j] * temp->b;
				temp++;
			}
			//Go to the start of the next row
			for (int j = 0; j < c - n; j++, temp++);
		}
		normalize_pix(pix, sum_r, sum_g, sum_b, g_sum);
	//	printf("%uc %uc %uc\n", pix->r, pix->g, pix->b);
	}
	else if (row + n1 < r && row - n1 >= 0) {
	//	printf("iterating (missing cols)\n");
		int lost_left = 0;
		int lost = (col + n1) - c + 1;
		if (lost <= 0) {
			lost = (n1 - col); 
			lost_left = 1;
		}
		int c_valid = n - lost;
		if (!lost_left) {
			g_sum = sum(gauss, 0, n, 0, c_valid);
		}
		else {
			g_sum = sum(gauss, 0, n, lost, n);
		}
		//Back up to the start of the valid matrix
		for (int i = 0; i < n1 * c + n1 - (lost * lost_left); i++, temp--);
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < c_valid; j++) {
				sum_r += gauss[i][j + (lost * lost_left)] * temp->r;		
				sum_g += gauss[i][j + (lost * lost_left)] * temp->g;		
				sum_b += gauss[i][j + (lost * lost_left)] * temp->b;
				temp++;	
			}
			//Go to the start of the next row
			for (int j = 0; j < c - c_valid; j++, temp++);
		}		
		normalize_pix(pix, sum_r, sum_g, sum_b, g_sum);
	}
	else {
		int lost_top = 0, lost_left = 0;
		int lost_r = (row + n1) - r + 1;
		if (lost_r <= 0) {
			lost_r = (n1 - row);
			lost_top = 1;	
		}
		int r_valid = n - lost_r;
		int lost_c = (col + n1) - c + 1;
		if (lost_c <= 0) {
			lost_c = (n1 - col); 
			lost_left = 1;
		}
		int c_valid = n - lost_c;
		if (lost_top) {
			if (lost_left) {
				g_sum = sum(gauss, 0, r_valid, 0, c_valid);
			}
			else {
				g_sum = sum(gauss, 0, r_valid, lost_c, n); 
			}
		}
		else {
			if (lost_left) {
				g_sum = sum(gauss, lost_r, n, 0, c_valid);
			}
			else {
				g_sum = sum(gauss, lost_r, n, lost_c, n);
			}

		}
		for (int i = 0; i < (n1 - (lost_r * lost_top)) * c + n1 - (lost_c * lost_left); i++, temp--);
		for (int i = 0; i < r_valid; i++) {
			for (int j = 0; j < c_valid; j++) {
				sum_r += gauss[i + (lost_r * lost_top)][j + (lost_c * lost_left)] * temp->r;		
				sum_g += gauss[i + (lost_r * lost_top)][j + (lost_c * lost_left)] * temp->g;		
				sum_b += gauss[i + (lost_r * lost_top)][j + (lost_c * lost_left)] * temp->b;
				temp++;	
			}
			//Go to the start of the next row
			for (int j = 0; j < c - c_valid; j++, temp++);
		}		
		normalize_pix(pix, sum_r, sum_g, sum_b, g_sum);	
	}

}

/** Function to blur an image
 *  @param im the image to blur
 *  @param sig the amount to blur the image by
 *  @return the blurred image
 */
img* blur(img *im, double sig) {
	int n = find_gauss_size(sig);
	double **gauss = create_gaussian(sig, n);
	img *temp = copy(im);
	int i = im->x * im-> y;
	Pixel *pix = im->pixels;
	Pixel *tmp = temp->pixels;
	for (int j = 0; j < i; j++, pix++, tmp++) {
		blur_pix(gauss, pix, j, im->x, im->y, n, tmp);
	}
	for (int k = 0; k < n; k++) {
		free(gauss[k]);
	}
	free(gauss);
	free(temp->pixels);
	free(temp);

	return im;	
}
