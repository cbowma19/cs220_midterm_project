/** Camille Bowman (cbowma19): cbowma19@jhu.edu
 *  William Hartmann (whartma2): whartma2@jhu.edu
 *
 *  601.220 Midterm Project due 10/16/2017
 *  Header file for img_manip.c
 */

#include "structs.h"

img* swap(img *im);

unsigned char adjust(unsigned char c, double amt);

img* brightness(img *im, double amt);

int find_gauss_size(double sig);

double** create_gaussian(double sig, int n);

double sum(double **avg, int rs, int rf, int cs, int cf);

void normalize_pix(Pixel *pix, double sum_r, double sum_g, double sum_b, double g_sum);

void blur_pix(double **gauss, Pixel *pix, int j, int c, int r, int n, Pixel *temp);

img* blur(img *im, double sig);

