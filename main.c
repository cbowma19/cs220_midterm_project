/** Camille Bowman (cbowma19): cbowma19@jhu.edu
 *  William Hartmann (whartma2): whartma2@jhu.edu
 *
 *  601.220 Midterm Project due 10/16/2017
 *  Driver file
 */

#include <stdio.h>
#include <string.h>
#include "menu_option.h"
#define LINE_SIZE 255


int main() {
	user_input();
	return 0;
}
