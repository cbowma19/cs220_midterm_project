/** Camille Bowman (cbowma19): cbowma19@jhu.edu
 *  William Hartmann (whartma2): whartma2@jhu.edu
 *
 *  601.220 Midterm Project due 10/16/2017
 *  File to process and error check user input 
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "read.h"
#include "write.h"
#include "menu_option.h"
#include "img_manip.h"
#include "structs.h"
#include "adjust.h"
#define LINE_SIZE 255
#define CORNERS 4

/** Prints list of available menu options
 */
void print_menu_options() {
	printf("Main menu:\n");
	printf("	r <filename> - read image from <filename>\n");
        printf("	w <filename> - write image to <filename>\n");
        printf("	s - swap color channels\n");
        printf("	br <amt> - change brightness (up or down) by the given amount\n");
        printf("	c <x1> <y1> <x2> <y2> - crop image to the box with the given corners\n");
        printf("	g - convert to grayscale\n");
        printf("	cn <amt> - change contrast (up or down) by the given amount\n");
        printf("	bl <sigma> - Gaussian blur with the given radius (sigma)\n");
        printf("	sh <sigma> <amt> - sharpen by given amount (intensity), with radius (sigma)\n");
        printf("	e <sigma> <threshold> - detect edges with intensity gradient above given threshold\n");
        printf("	q - quit\n");
}

/** Directs user choice to the correct function
 *  @param str the user input
 *  @param im pointer to the image
 *  @return the image pointer
 */
img * menu_option(char *str, img *im) {
	char *command, *file, *sigma, *amount, *err;
	double corners[CORNERS];
	double sig, amt;
	if (!im->valid && str[0] != 'r') {
		fprintf(stderr, "No image file has been read yet\n");
		return im;
	}
	switch (str[0]) {
		case 'r' :
			command = strtok(str, " ");
			file = strtok(NULL, "\0");
			if (file != NULL) {
				FILE *fp = fopen(file, "r");
				if (fp == NULL) {
					fprintf(stderr, "%s is an invalid file name\n", file);
				}
				else {
					im = read(im, fp);
					im->valid = 1;
					fclose(fp);
				}
			}
			else {
				fprintf(stderr, "No file name was inputted\n");
			}
			break;
		case 'w' :		
			command = strtok(str, " ");
			file = strtok(NULL, "\0");
			if (file != NULL) {
				FILE *fp = fopen(file, "w");
				if (fp == NULL) {
					fprintf(stderr, "%s is an invalid file name\n", file);
				}
				else {
					write(fp, im);	
				}
			}
			else {
				fprintf(stderr, "No file name was inputted\n");
			}
			break;
		case 's' :
			if (str[1] == 'h') {
				command = strtok(str, " ");
				sigma = strtok(NULL, " ");
				amount = strtok(NULL, "\0");
				if (sigma == NULL) {
					fprintf(stderr, "Invalid number of inputs\n");
				}
				else if (amount == NULL) {
					fprintf(stderr, "Invalid number of inputs\n");
				}
				else {
					sig = strtof(sigma, &err);
					int count = 0;
					if (*err != 0) {
						fprintf(stderr, "%s is in invalid format\n", sigma);
						count++;
					}
					amt = strtof(amount, &err);
					if (*err != 0) {
						fprintf(stderr, "%s is in invalid format\n", amount);
						count++;
					}
					if (count == 0) {
						sharpen(im, sig, amt);
					}	
				}
			}
			else {
				if (str[1] == '\0') {
					im = swap(im);
				}
				else {
					fprintf(stderr, "Too many arguments\n");
				}
			}
			break;
		case 'b' :
			command = strtok(str, " ");
			amount = strtok(NULL, "\0");
			if (amount == NULL) {
				fprintf(stderr, "Invalid number of inputs\n");
				return im;
			}
			amt = strtof(amount, &err);
			if (*err != 0) { 
				fprintf(stderr, "%s is in the incorrect format\n", amount);
			}
			else {
				if (strcmp(command, "bl") == 0) {
					blur(im, amt);	
				}
				else {
					im = brightness(im, amt);	
				}
			}
			break;
		case 'c' :
			command = strtok(str, " ");
			if (strcmp(command, "cn") == 0) {
				amount = strtok(NULL, "\0");
				if (amount == NULL) {
					fprintf(stderr, "Invalid amount of inputs\n");
					return im;
				}
				amt = strtof(amount, &err);
				if (*err != 0) {
					fprintf(stderr, "%s is in invalid format\n", amount);
				}
				else {
					im = contrast(amt, im);	
				}
			}
			else {
				int count = 0;
				for (int i = 0; i < CORNERS; i++) {
					sigma = strtok(NULL, " ");
					if (sigma == NULL) {
						fprintf(stderr, "Invalid amount of inputs\n");
						count++;	
						continue;
					}
					corners[i] = strtof(sigma, &err);
					if (*err != 0) {
						fprintf(stderr, "%s is in invalid format\n", sigma);
						count++;	
						continue;
					}
				}	
				if (count == 0) {
					im = crop(im, corners[0], corners[1], corners[2], corners[3]);	
				}
			}
			break;
		case 'g' :	
			if (strlen(str) == 1) {
				im = grayscale(im);
			}
			else {
				fprintf(stderr, "Too many inputs\n");
			}
			break;
		case 'e' :	
			command = strtok(str, " ");
			sigma = strtok(NULL, " ");
			char *threshold = strtok(NULL, "\0");
			if (sigma == NULL) {
				fprintf(stderr, "Invalid number of inputs\n");
			}
			else if (threshold == NULL) {
				fprintf(stderr, "Invalid number of inputs\n");
			}
			else {
				int count = 0;
				sig = strtof(sigma, &err);
				if (*err != 0) {
					fprintf(stderr, "%s is in invalid format\n", sigma);
					count++;
				}
				double thresh = strtof(threshold, &err);
				if (*err != 0) {
					fprintf(stderr, "%s is in invalid format\n", threshold);
					count++;
				}
				if (count == 0) {	
					edge(im, sig, thresh);	
				}
			}
			break;
		default :
			fprintf(stderr, "%s is an invalid menu option\n", str);
	}
	return im;
}

void user_input() {
	print_menu_options();
	char *line[LINE_SIZE], line1[LINE_SIZE];
	img *im = malloc(sizeof(img));
	im->valid = 0;
	fgets(line1, LINE_SIZE, stdin);
	*line = strtok(line1, "\n");
	while (strcmp(*line, "q") != 0) {
		im = menu_option(*line, im);
		print_menu_options();
		fgets(line1, LINE_SIZE, stdin);
		*line = strtok(line1, "\n");
	}
	free(im->pixels);
	free(im);
}
