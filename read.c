/** Camille Bowman (cbowma19): cbowma19@jhu.edu
 *  William Hartmann (whartma2): whartma2@jhu.edu
 *
 *  601.220 Midterm Project due 10/16/2017
 *  Function to read in an image from a file 
 */

#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include "read.h"
#include "structs.h"

img* read(img *myimg, FILE *readf){
  
  //FILE *fp = = fopen(filename, "r");
  char sig[16];
  if(fgets(sig, sizeof(sig), readf) != NULL){
    if((sig[0] != 'P') || (sig[1] != '6')){
      fprintf(stderr, "Bad ppm format\n");
      return NULL;
    }else{
      char c = getc(readf);
      while(c == '#'){
      // int end = 0;
	char q = getc(readf);
	while(q != '\n'){
	  q = getc(readf);
	}
	c = getc(readf);
      }
      ungetc(c, readf);
   
      int rows = 0;
      int columns = 0;
      int colors = 0;
      fscanf(readf," %d",&columns);
      getc(readf);
      fscanf(readf," %d",&rows);
      getc(readf);
      fscanf(readf," %d",&colors);
      getc(readf); 
    //   img* myimg = (img*)malloc(sizeof(img));
      myimg->pixels = (Pixel *)realloc(myimg->pixels,rows*columns*sizeof(Pixel));
    //free(myimg->pixels);
    //    myimg->pixels = malloc(rows*columns*sizeof(Pixel));
      myimg->x = columns;
      myimg->y = rows;
      fread(myimg->pixels,3,rows*columns,readf);
      return myimg;
    }
  }else{
    fprintf(stderr, "Serious problem\n");
    return NULL;
  }
}
