/** Camille Bowman (cbowma19): cbowma19@jhu.edu
 *  William Hartmann (whartma2): whartma2@jhu.edu
 *
 *  601.220 Midterm Project due 10/16/2017
 *  Header file for read.c 
 */

#include "structs.h"

img* read(img *myimg, FILE *readf);

