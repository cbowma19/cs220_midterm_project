/** Camille Bowman (cbowma19): cbowma19@jhu.edu
 *  William Hartmann (whartma2): whartma2@jhu.edu
 *
 *  601.220 Midterm Project due 10/16/2017
 *  Header file for structs used in image manipulation 
 */

#ifndef STRUCTS_H
#define STRUCTS_H

/** Structure to contain information about one pixel
 *  @param r the pixel's red value
 *  @param g the pixel's green value
 *  @param b the pixel's blue value
 */
typedef struct {
unsigned  char r,g,b;
}Pixel;

/** Struture to contain information about an image 
 *  @param x the number of columns of pixels
 *  @param y the number of rows of pixels
 */
typedef struct {
  int x,y, valid;
  Pixel* pixels;
}img;


#endif /* STRUCTS_H */
