/** Camille Bowman (cbowma19): cbowma19@jhu.edu
 *  William Hartmann (whartma2): whartma2@jhu.edu
 *
 *  601.220 Midterm Project due 10/16/2017
 *  Function to write an image to a file 
 */

#include <stdlib.h>
#include <stdio.h>
#include "write.h"
#include "structs.h"

/** Writes an image to a file
 *  @param writef the file to write to
 *  @param towrite the image to write
 */
void write(FILE *writef, img *towrite){
  fprintf(writef,"P6\n");
  fprintf(writef,"%d %d\n255\n",towrite->x,towrite->y);
  fwrite(towrite->pixels,3,towrite->x * towrite->y,writef);
}
