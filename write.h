/** Camille Bowman (cbowma19): cbowma19@jhu.edu
 *  William Hartmann (whartma2): whartma2@jhu.edu
 *
 *  601.220 Midterm Project due 10/16/2017
 *  Header file for write.c 
 */

#include "structs.h"

void write(FILE *writef, img *towrite);

